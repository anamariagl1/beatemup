using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;

public class PoolBalasController : MonoBehaviour
{
    public void CrearBalas(int atk, Transform t)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            Transform bala = this.transform.GetChild(i);

            if (bala.gameObject.activeInHierarchy == false)
            {
                bala.gameObject.SetActive(true);
                bala.gameObject.GetComponent<BalaController>().ReiniciarBala(atk);
                bala.transform.position = new Vector2(t.position.x, t.position.y+1);
                bala.GetComponent<Rigidbody2D>().velocity = t.right * 4;
                break;
            }
        }
    }
}
