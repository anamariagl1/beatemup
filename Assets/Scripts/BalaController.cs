using System;
using UnityEngine;

public class BalaController : MonoBehaviour
{
    public int atk;
    private int m_PlayerLayer;

    private void Start()
    {
        m_PlayerLayer = LayerMask.NameToLayer("Player");
        atk = 0;
    }

    private void Update()
    {
        if (this.transform.position.x > 10 || this.transform.position.x < -10 || this.transform.position.y > 7 || this.transform.position.y < -7)
        {
            this.gameObject.SetActive(false);
        }
    }
    public void ReiniciarBala(int atk)
    {
        this.atk = atk;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerLayer)
        {
            this.gameObject.SetActive(false);
        }
    }
}
