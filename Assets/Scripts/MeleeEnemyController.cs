using System.Collections;
using UnityEngine;

public class MeleeEnemyController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT };
    private SwitchMachineStates m_CurrentState;
    private Rigidbody2D m_Rigidbody;
    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private HitboxController m_HitboxController;
    [SerializeField]
    private GameObject m_Player;
    [SerializeField]
    private SO_Enemy m_EnemyStats;
    public SO_Enemy EnemyStats => m_EnemyStats;
    [SerializeField]
    private int m_Hp;
    [SerializeField]
    private DetectionAreaController m_DetectionArea;
    [SerializeField]
    private DetectionAreaController m_AttackArea;
    private int m_PlayerAttacksLayer;

    private bool m_Cooldown;
    [SerializeField]
    private GameEvent m_ImDeadEvent;

    private void Awake()
    {

        m_Rigidbody = this.GetComponent<Rigidbody2D>();
        m_Animator = this.GetComponent<Animator>();
        m_Hp = m_EnemyStats.hp;

        Init(m_EnemyStats);

    }
    public void Start()
    {
       // InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnEnable()
    {
        m_DetectionArea.OnDetectPlayer += PlayerDetected;
        m_AttackArea.OnDetectPlayer += PlayerInRange;
        m_AttackArea.OnPlayerOutOfRange += PlayerOutOfRange;

        m_PlayerAttacksLayer = LayerMask.NameToLayer("PlayerAttacks");
        m_Player = GameObject.Find("Player");

        InitState(SwitchMachineStates.IDLE);

    }

    private void OnDestroy()
    {
        m_DetectionArea.OnDetectPlayer -= PlayerDetected;
        m_AttackArea.OnDetectPlayer -= PlayerDetected;
        m_AttackArea.OnPlayerOutOfRange -= PlayerOutOfRange;
    }

    private void PlayerDetected()
    {
        ChangeState(SwitchMachineStates.WALK);
    }

    private void PlayerInRange()
    {
        ChangeState(SwitchMachineStates.HIT);
    }

    private void PlayerOutOfRange()
    {
        //print("out range");
        ChangeState(SwitchMachineStates.WALK);
    }

    public void Init(SO_Enemy enemy)
    {
        m_EnemyStats = enemy;
        m_Hp = m_EnemyStats.hp;

        float x = Random.Range(0, 8);
        float y = Random.Range(-4, 2.5f);
        this.transform.position = new Vector2(x, y);
        this.transform.right = -Vector3.right;

        m_Cooldown = false;

        this.gameObject.SetActive(true);
        InitState(SwitchMachineStates.IDLE);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerAttacksLayer)
        {
            LoseHealth(collision.gameObject.GetComponent<HitboxController>().atk);
        }
    }

    private void LoseHealth(int dmg)
    {
        print("MeleeEnemy pierde " + dmg + " hp");
        m_Hp -= dmg;
        if (m_Hp <= 0)
        {
            m_ImDeadEvent.Raise();
            StopAllCoroutines();
            m_DetectionArea.OnDetectPlayer -= PlayerDetected;
            m_AttackArea.OnDetectPlayer -= PlayerDetected;
            m_AttackArea.OnPlayerOutOfRange -= PlayerOutOfRange;
            this.gameObject.SetActive(false);
        }
    }

    private IEnumerator Cooldown()
    {
        m_Cooldown = true;
        yield return new WaitForSeconds(1f);
        m_Cooldown = false;
    }

    private void Encarar()
    {
        if (m_Player.gameObject.transform.position.x - this.transform.position.x > 0)
        {
            this.gameObject.transform.eulerAngles = Vector2.zero;
        }
        else if (m_Player.gameObject.transform.position.x - this.transform.position.x < 0)
        {
            this.gameObject.transform.eulerAngles = new Vector2(0, 180);
        }
    }

    /******************** STATE MACHINE *********************/

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("MeleeEnemy_Idle");
                break;

            case SwitchMachineStates.WALK:
                m_Animator.Play("MeleeEnemy_Walk");
                break;

            case SwitchMachineStates.HIT:
                Encarar();
                break;


            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:
                m_Rigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.HIT:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.WALK:
                Vector2 direction = (m_Player.transform.position - this.transform.position).normalized;
                m_Rigidbody.velocity = direction * m_EnemyStats.spd;

                Encarar();
                break;

            case SwitchMachineStates.HIT:
                if (!m_Cooldown)
                {
                    print("hit");
                    m_Rigidbody.velocity = Vector2.zero;
                    m_Animator.Play("MeleeEnemy_Attack", 0, 0f);
                    StartCoroutine(Cooldown());
                }
                break;

            default:
                break;
        }
    }
}
