using UnityEngine;

public class OleadaGameOver : MonoBehaviour
{
    [SerializeField]
    private SO_Partida m_Partida;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = "Oleadas sobrevividas: " + m_Partida.oleadaActual;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
