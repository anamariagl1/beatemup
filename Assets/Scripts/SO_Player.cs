using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Player : ScriptableObject
{
    public int hp; //Vida del player
    public int atk; //Da�o del ataque b�sico del player
    public int atk2; //Da�o del ataque fuerte del player
    public int spd; //Velocidad de movimiento del player

}
