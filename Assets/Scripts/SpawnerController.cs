using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnerController : MonoBehaviour
{
    //WAVES
    [SerializeField]
    private SO_Partida m_Partida;
    [SerializeField]
    private SO_Wave[] m_Waves;

    //SO ENEMIES
    [SerializeField]
    private SO_Enemy m_Melee;
    [SerializeField]
    private SO_Enemy m_Ranged;
    [SerializeField]
    private SO_Enemy m_MeleeStrong;
    [SerializeField]
    private SO_Enemy m_RangedStrong;

    //PREFABS
    [SerializeField]
    private GameObject m_MeleeEnemy;
    [SerializeField]
    private GameObject m_RangedEnemy;

    [SerializeField]
    private Transform[] m_Rutas;
    [SerializeField]
    private PoolBalasController m_Balas;

    [SerializeField]
    private int m_EnemyCount;
    [SerializeField]
    private int m_MaxEnemies;

    [SerializeField]
    private GameEventInt m_NewWaveEvent;

    private void Awake()
    {
        m_Partida.oleadaActual = 0;
        GenerateEnemies();
    }

    void Start()
    {
        SpawnEnemies(m_Waves[m_Partida.oleadaActual]);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void GenerateEnemies()
    {
        for (int i = 0; i < m_MaxEnemies; i++)
        {
            Instantiate(m_RangedEnemy, this.transform);
            Instantiate(m_MeleeEnemy, this.transform);
        }
    }

    public void RestarEnemigo()
    {
        m_EnemyCount--;
        if (m_EnemyCount <= 0)
        {
            NewWave();
        }
    }
    private void NewWave()
    {
        m_Partida.oleadaActual++;
        if (m_Partida.oleadaActual < m_Waves.Length)
        {
            m_NewWaveEvent.Raise(m_Partida.oleadaActual);
            SpawnEnemies(m_Waves[m_Partida.oleadaActual]);
        }
        else
        {
            Victory();
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOverScene");
    }
    public void Victory()
    {
        SceneManager.LoadScene("VictoryScene");
    }
    private void SpawnEnemies(SO_Wave wave)
    {
        m_EnemyCount = wave.numEnemigos;
        int contMelee = (wave.porcentajeMelee * wave.numEnemigos) / 100;
        int contRanged = wave.numEnemigos - contMelee;
        int contMeleeActual = 0;
        int contRangedActual = 0;

        for (int i = 0; i < this.transform.childCount; i++)
        {
            GameObject enemy = this.transform.GetChild(i).gameObject;
            if (contMelee > contMeleeActual && enemy.tag == "MeleeEnemy" && !enemy.activeInHierarchy)
            {
                switch (wave.tiposEnemigosMelee)
                {
                    case TiposEnemigos.tiposEnemigos.NORMAL:
                        enemy.GetComponent<MeleeEnemyController>().Init(m_Melee);
                        enemy.GetComponent<SpriteRenderer>().color = Color.white;
                        break;

                    case TiposEnemigos.tiposEnemigos.FUERTE:
                        enemy.GetComponent<MeleeEnemyController>().Init(m_MeleeStrong);
                        enemy.GetComponent<SpriteRenderer>().color = Color.blue;
                        break;
                }
                contMeleeActual++;
            }
            if (contRanged > contRangedActual && enemy.tag == "RangedEnemy" && !enemy.activeInHierarchy)
            {
                switch (wave.tiposEnemigosRanged)
                {
                    case TiposEnemigos.tiposEnemigos.NORMAL:
                        enemy.GetComponent<RangedEnemyController>().Init(m_Ranged, m_Rutas, m_Balas);
                        enemy.GetComponent<SpriteRenderer>().color = Color.white;
                        break;

                    case TiposEnemigos.tiposEnemigos.FUERTE:
                        enemy.GetComponent<RangedEnemyController>().Init(m_RangedStrong, m_Rutas, m_Balas);
                        enemy.GetComponent<SpriteRenderer>().color = Color.blue;
                        break;
                }
                contRangedActual++;
            }
        }
    }


}
