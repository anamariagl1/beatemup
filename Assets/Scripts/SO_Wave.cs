using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Wave : ScriptableObject
{
    public int numEnemigos;
    public int porcentajeMelee;
    public TiposEnemigos.tiposEnemigos tiposEnemigosMelee;
    public TiposEnemigos.tiposEnemigos tiposEnemigosRanged;
}
