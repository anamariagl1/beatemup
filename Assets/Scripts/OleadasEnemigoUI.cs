using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OleadasEnemigoUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActualizarOleadas(int oleada)
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Oleada: " + (oleada+1);
    }
}
