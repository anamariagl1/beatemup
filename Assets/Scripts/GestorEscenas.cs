using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GestorEscenas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Nivel()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOverScene");
    }

    public void Victoria()
    {
        SceneManager.LoadScene("VictoryScene");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("CreditsScene");
    }
}
