using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionAreaController : MonoBehaviour
{

    public delegate void DetectPlayer();
    public event DetectPlayer OnDetectPlayer;
    public delegate void PlayerOutOfRange();
    public event PlayerOutOfRange OnPlayerOutOfRange;
    private int m_PlayerLayer;
    // Start is called before the first frame update
    void Start()
    {
        m_PlayerLayer = LayerMask.NameToLayer("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        m_PlayerLayer = LayerMask.NameToLayer("Player");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.name);
        if(collision.gameObject.layer == m_PlayerLayer)
        {
            OnDetectPlayer?.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerLayer)
        {
            OnPlayerOutOfRange?.Invoke();
        }
    }
}
