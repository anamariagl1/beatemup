// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
    public List<GameEventListener> events = new List<GameEventListener>();
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>

    public void Raise()
    {
        for(int i=events.Count-1;i>=0;i--)
        {
            events[i].OnEventRaised();
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (!events.Contains(listener))
            events.Add(listener);
    }

    public void UnregisterListener(GameEventListener listener)
    {
        if (events.Contains(listener))
            events.Remove(listener);
    }
}

