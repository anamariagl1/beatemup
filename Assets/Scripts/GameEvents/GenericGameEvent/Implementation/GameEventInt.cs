using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - Int")]
    public class GameEventInt : GameEvent<int> { }

