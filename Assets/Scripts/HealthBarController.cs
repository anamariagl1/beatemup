using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{

    [SerializeField]
    private SO_Player m_PlayerStats;

    private void Awake()
    {
        this.gameObject.GetComponent<Slider>().value = 1;
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void actualizarVida(int hp)
    {
        float fillValue = (float) hp / (float) m_PlayerStats.hp;
        this.gameObject.GetComponent<Slider>().value = fillValue;
    }
}
