using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Enemy : ScriptableObject
{
    public int hp; 
    public int atk;
    public int spd;
}
