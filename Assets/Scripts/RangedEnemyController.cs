using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, PATROL, CHASE, SHOOT, IDLE };
    private SwitchMachineStates m_CurrentState;
    private Rigidbody2D m_Rigidbody;
    [SerializeField] private Animator m_Animator;
    [SerializeField] private bool m_Persiguiendo;
    [SerializeField] private GameObject m_Player;
    [SerializeField] private SO_Enemy m_EnemyStats;
    public SO_Enemy EnemyStats => m_EnemyStats;
    [SerializeField]
    private int m_Hp;
    [SerializeField] private DetectionAreaController m_DetectionArea;
    [SerializeField] private DetectionAreaController m_AttackArea;
    private int m_PlayerAttacksLayer;
    [SerializeField] private PoolBalasController m_PoolBalas;
    [SerializeField] private Transform[] m_Waypoints;
    private int m_WaypointActual;
    [SerializeField] private GameEvent m_ImDeadEvent;

    public void Awake()
    {
        m_DetectionArea.OnDetectPlayer += PlayerDetected;
        m_DetectionArea.OnPlayerOutOfRange += PlayerOutOfDetection;
        m_AttackArea.OnDetectPlayer += PlayerInRange;
        m_AttackArea.OnPlayerOutOfRange += PlayerOutOfRange;

        m_PlayerAttacksLayer = LayerMask.NameToLayer("PlayerAttacks");
        m_Player = GameObject.Find("Player");
        m_Rigidbody = this.GetComponent<Rigidbody2D>();
        m_Animator = this.GetComponent<Animator>();
        m_Hp = m_EnemyStats.hp;

        m_Persiguiendo = false;
    }

    private void Start()
    {
        //InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        m_DetectionArea.OnDetectPlayer -= PlayerDetected;
        m_DetectionArea.OnPlayerOutOfRange -= PlayerOutOfDetection;
        m_AttackArea.OnDetectPlayer -= PlayerDetected;
        m_AttackArea.OnPlayerOutOfRange -= PlayerOutOfRange;
    }

    private void PlayerDetected()
    {
        m_Persiguiendo = true;
        ChangeState(SwitchMachineStates.CHASE);
    }

    private void PlayerOutOfDetection()
    {
        m_Persiguiendo = false;
        ChangeState(SwitchMachineStates.PATROL);
    }

    private void PlayerInRange()
    {
        ChangeState(SwitchMachineStates.SHOOT);
    }

    private void PlayerOutOfRange()
    {
        StopAllCoroutines();
        ChangeState(SwitchMachineStates.CHASE);
    }

    public void Init(SO_Enemy enemy, Transform[] rutas, PoolBalasController m_Balas)
    {
        m_EnemyStats = enemy;
        m_Hp = m_EnemyStats.hp;
        m_Persiguiendo = false;
        m_Waypoints = rutas;
        m_PoolBalas = m_Balas;

        float x = Random.Range(0, 8);
        float y = Random.Range(-4, 2.5f);
        this.transform.position = new Vector2(x, y);
        this.transform.right = -Vector3.right;

        m_WaypointActual = Random.Range(0, m_Waypoints.Length);

        this.gameObject.SetActive(true);
        InitState(SwitchMachineStates.PATROL);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_PlayerAttacksLayer)
        {
            LoseHealth(collision.gameObject.GetComponent<HitboxController>().atk);
        }
    }

    private void LoseHealth(int dmg)
    {
        print("Ranged Enemy pierde " + dmg + " hp");
        m_Hp -= dmg;
        if (m_Hp <= 0)
        {
            m_ImDeadEvent.Raise();
            StopAllCoroutines();            
            this.gameObject.SetActive(false);
        }
    }

    private IEnumerator Disparar()
    {
        while (true)
        {
            Encarar();
            m_PoolBalas.CrearBalas(m_EnemyStats.atk, this.transform);
            yield return new WaitForSeconds(1);
        }
    }
    private void Encarar()
    {
        if (m_Player.gameObject.transform.position.x - this.transform.position.x > 0)
        {
            this.gameObject.transform.eulerAngles = Vector2.zero;
        }
        else if (m_Player.gameObject.transform.position.x - this.transform.position.x < 0)
        {
            this.gameObject.transform.eulerAngles = new Vector2(0, 180);
        }
    }

    private void Patrullar()
    {
        Vector3 newWaypoint = m_Waypoints[m_WaypointActual].position;
        float distancia = Vector3.Distance(newWaypoint, this.transform.position);

        if (distancia < 1.5f)
        {
            m_WaypointActual = (m_WaypointActual + 1) % m_Waypoints.Length;
            ChangeState(SwitchMachineStates.IDLE);
        }
        else
        {
            Vector3 m_VectorDistancia = (newWaypoint - this.transform.position).normalized;
            m_Rigidbody.velocity = m_VectorDistancia * m_EnemyStats.spd;
        }
    }

    private IEnumerator Esperar()
    {
        yield return new WaitForSeconds(2);
        ChangeState(SwitchMachineStates.PATROL);
    }

    /******************** STATE MACHINE *********************/

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        if (this.gameObject.activeSelf)
        {
            m_CurrentState = currentState;
            switch (m_CurrentState)
            {
                case SwitchMachineStates.IDLE:
                    m_Rigidbody.velocity = Vector3.zero;
                    StartCoroutine(Esperar());
                    m_Animator.Play("RangedEnemy_Idle");
                    break;

                case SwitchMachineStates.PATROL:
                    Patrullar();
                    m_Animator.Play("RangedEnemy_Walk");
                    break;

                case SwitchMachineStates.CHASE:
                    Encarar();
                    m_Animator.Play("RangedEnemy_Walk");
                    break;

                case SwitchMachineStates.SHOOT:
                    m_Rigidbody.velocity = Vector3.zero;
                    m_Rigidbody.velocity = Vector2.zero;
                    m_Animator.Play("RangedEnemy_Shoot");
                    StartCoroutine(Disparar());
                    break;

                default:
                    break;
            }
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                StopAllCoroutines();
                break;

            case SwitchMachineStates.PATROL:
                m_Rigidbody.velocity = Vector3.zero;
                break;

            case SwitchMachineStates.CHASE:
                m_Rigidbody.velocity = Vector3.zero;
                break;

            case SwitchMachineStates.SHOOT:
                StopAllCoroutines();
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.PATROL:
                if (m_Persiguiendo)
                {
                    ChangeState(SwitchMachineStates.CHASE);
                }
                else
                {
                    Patrullar();
                }
                break;

            case SwitchMachineStates.CHASE:
                Vector2 direction = (m_Player.transform.position - this.transform.position).normalized;
                m_Rigidbody.velocity = direction * m_EnemyStats.spd;
                Encarar();                

                break;

            case SwitchMachineStates.SHOOT:
                Encarar();

                break;

            default:
                break;
        }
    }
}
