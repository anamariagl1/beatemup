using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;

    [SerializeField]
    private SO_Player m_PlayerStats;
    private Rigidbody2D m_Rigidbody;
    private Animator m_Animator;
    private bool m_ComboWindow; //Time after first attack where you can make a combo
    [SerializeField]
    private HitboxController m_HitboxController;


    [SerializeField]
    private int m_Hp;
    [SerializeField]
    private int m_Atk1; //Attack damage from basic attack
    [SerializeField]
    private int m_Atk2; //Attack damage from strong attack
    private int m_EnemyAttacksLayer;

    [SerializeField]
    private GameEventInt m_PlayerHurt;
    [SerializeField]
    private GameEvent m_PlayerDeadEvent;

    private enum SwitchMachineStates { NONE, IDLE, WALK, HIT1, HIT2, COMBO, HURT };
    private SwitchMachineStates m_CurrentState;

    // Start is called before the first frame update

    private void Awake()
    {
        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Attack").performed += AttackAction;
        m_Input.FindActionMap("Default").FindAction("StrongAttack").performed += StrongAttackAction;
        m_Input.FindActionMap("Default").Enable();

        m_Atk1 = m_PlayerStats.atk;
        m_Atk2 = m_PlayerStats.atk2;
        m_EnemyAttacksLayer = LayerMask.NameToLayer("EnemyAttacks");
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_ComboWindow = false;
        m_Hp=m_PlayerStats.hp;
    }
    void Start()
    {
        NewWaveInit();
        InitState(SwitchMachineStates.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }



    private void OnDestroy()
    {
        m_Input.FindActionMap("Default").FindAction("Attack").performed -= AttackAction;
        m_Input.FindActionMap("Default").FindAction("StrongAttack").performed -= StrongAttackAction;
        m_Input.FindActionMap("Default").Disable();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == m_EnemyAttacksLayer)
        {
            if (collision.gameObject.GetComponent<HitboxController>()!=null)
            {
                LoseHealth(collision.gameObject.GetComponent<HitboxController>().atk);
            }
            else
            {
                LoseHealth(collision.gameObject.GetComponent<BalaController>().atk);
            }
        }
    }

    private void LoseHealth(int dmg)
    {
        ChangeState(SwitchMachineStates.HURT);
        print("Player pierde " + dmg + " hp");
        m_Hp -= dmg;

        if(m_Hp <= 0)
        {
            m_PlayerDeadEvent.Raise();
            //this.gameObject.SetActive(false);
        }
    }

    public void NewWaveInit()
    {
        this.transform.position = new Vector2(-6, 0);
        this.transform.right = Vector3.right;
        InitState(SwitchMachineStates.IDLE);
    }



    /******************** STATE MACHINE *********************/

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                m_Animator.Play("Player_Idle");
                break;

            case SwitchMachineStates.WALK:
                m_Animator.Play("Player_Walk");
                break;

            case SwitchMachineStates.HIT1:
                m_Rigidbody.velocity = Vector2.zero;
                m_HitboxController.atk = m_Atk1;
                m_Animator.Play("Player_Attack");
                InitComboWindow();
                break;

            case SwitchMachineStates.HIT2:
                m_Rigidbody.velocity = Vector2.zero;
                m_HitboxController.atk = m_Atk2;
                m_Animator.Play("Player_StrongAttack");
                InitComboWindow();
                break;

            case SwitchMachineStates.HURT:
                m_Animator.Play("Player_Hurt");
                m_PlayerHurt.Raise(m_Hp);
                break;

            case SwitchMachineStates.COMBO:
                m_HitboxController.atk = m_Atk2 * 2;
                m_Animator.Play("Player_Combo");
                EndComboWindow();
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.HIT1:
                break;

            case SwitchMachineStates.HIT2:
                break;

            case SwitchMachineStates.HURT:

                break;

            case SwitchMachineStates.COMBO:

                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().x != 0 || m_MovementAction.ReadValue<Vector2>().y != 0)
                    ChangeState(SwitchMachineStates.WALK);
                break;

            case SwitchMachineStates.WALK:

                m_Rigidbody.velocity = m_MovementAction.ReadValue<Vector2>() * m_PlayerStats.spd;
                if (m_MovementAction.ReadValue<Vector2>().x > 0)
                {
                    this.gameObject.transform.eulerAngles = Vector3.zero;
                }
                else if (m_MovementAction.ReadValue<Vector2>().x < 0)
                {
                    this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
                }

                if (m_Rigidbody.velocity == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);

                break;
            case SwitchMachineStates.HIT1:

                break;

            case SwitchMachineStates.HIT2:

                break;

            case SwitchMachineStates.HURT:

                break;

            case SwitchMachineStates.COMBO:

                break;

            default:
                break;
        }
    }


    private void AttackAction(InputAction.CallbackContext context) //Player attacks with a basic attack
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HIT1);

                break;

            case SwitchMachineStates.HIT2:
                if (m_ComboWindow)
                {
                    ChangeState(SwitchMachineStates.COMBO);
                }
                else
                {
                    ChangeState(SwitchMachineStates.HIT1);
                }

                break;

            case SwitchMachineStates.HIT1:
                ChangeState(SwitchMachineStates.HIT1);
                break;

            case SwitchMachineStates.HURT:

                break;

            case SwitchMachineStates.COMBO:

                break;
            default:
                break;
        }

    }

    private void StrongAttackAction(InputAction.CallbackContext context) //Player attacks with a strong attack
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                ChangeState(SwitchMachineStates.HIT2);

                break;

            case SwitchMachineStates.WALK:
                ChangeState(SwitchMachineStates.HIT2);

                break;

            case SwitchMachineStates.HIT1:
                if (m_ComboWindow)
                {
                    ChangeState(SwitchMachineStates.COMBO);
                }
                else
                {
                    ChangeState(SwitchMachineStates.HIT2);
                }
                break;

            case SwitchMachineStates.HIT2:
                ChangeState(SwitchMachineStates.HIT2);
                break;

            case SwitchMachineStates.HURT:

                break;

            case SwitchMachineStates.COMBO:

                break;

            default:
                break;
        }

    }

    public void InitComboWindow()
    {
        StartCoroutine(ComboWindow());
    }

    public void EndComboWindow()
    {
        m_ComboWindow = false;
    }

    private IEnumerator ComboWindow()
    {
        m_ComboWindow = true;
        yield return new WaitForSeconds(0.8f);
        EndComboWindow();
    }

}
